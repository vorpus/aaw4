class ChangeBonusTracksInTracksNull < ActiveRecord::Migration[5.0]
  def change
    change_column :tracks, :bonus_track, :boolean, null: true
  end
end
