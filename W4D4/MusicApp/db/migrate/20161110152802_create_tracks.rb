class CreateTracks < ActiveRecord::Migration[5.0]
  def change
    create_table :tracks do |t|
      t.string :track_name, null: false
      t.integer :album_id, null: false
      t.boolean :bonus_track, null: false, default: false
      t.string :lyrics, null: false
    end
    add_index :tracks, :album_id
  end
end
