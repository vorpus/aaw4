Rails.application.routes.draw do
  root to: "bands#index"
  resources :users, only: [:create, :new, :show]
  resource :session, only: [:create, :new, :destroy]
  resources :bands do
    resources :albums, only: :new
  end

  resources :albums, only: [:create, :update, :destroy, :edit, :show] do
    resources :tracks, only: :new
  end

  # resources :albums, only: [:create, :update, :destroy, :edit, :show]
  resources :tracks, only: [:create, :update, :destroy, :edit, :show]

  resources :notes, only: [:create, :update, :destroy]
end
