class NotesController < ApplicationController
  def create
    note = Note.new(notes_params)
    note.user_id = current_user.id
    if note.save
      redirect_to track_url(note.track)
    else
      flash[:errors] = note.errors.full_messages
      redirect_to track_url(note.track)
    end
  end

  def destroy
    note = Note.find(params[:id])
    if current_user.id == note.user_id
      note.destroy
      redirect_to track_url(note.track)
    else
      render text: "absolutely not", status: 403
    end
  end

  private
  def notes_params
    params.require(:notes).permit(:note_body, :track_id)
  end
end
