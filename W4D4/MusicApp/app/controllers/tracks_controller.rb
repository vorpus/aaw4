class TracksController < ApplicationController
  def new

    render :new
  end

  def create
    track = Track.new(tracks_params)
    if track.save
      redirect_to track_url(track)
    else
      flash[:errors] = track.errors.full_messages
      redirect_to album_url(track.album_id)
    end
  end

  def show
    @track = Track.find(params[:id])
    render :show
  end

  def edit
    @track = Track.find(params[:id])
    render :edit
  end

  def update
    @track = Track.find(params[:id])
    if @track.update(tracks_params)
      render :show
    else
      flash[:errors] = @track.errors.full_messages
      redirect_to track_url(@track)
    end
  end

  def destroy
    @track = Track.find(params[:id])
    if @track.destroy
      redirect_to album_url(@track.album_id)
    else
      flash[:errors] = @track.errors.full_messages
      redirect_to track_url(@track)
    end
  end

  private
  def tracks_params
    params.require(:tracks).permit(:album_id, :track_name, :lyrics)
  end
end
