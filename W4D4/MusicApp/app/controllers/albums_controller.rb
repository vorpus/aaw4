class AlbumsController < ApplicationController

  def new
    render :new
  end


  def show
    @album = Album.find(params[:id])
    render :show
  end

  def create
    album = Album.new(album_params)
    puts album
    if album.save
      redirect_to album_url(album)
    else
      redirect_to band_url(album.band_id)
    end
  end

  def edit
    @album = Album.find(params[:id])
    render :edit
  end

  def update
    album = Album.find(params[:id])
    if album.update(album_params)
      redirect_to album_url(album)
    else
      flash[:errors] = album.errors.full_messages
      redirect_to album_url(album)
    end
  end

  def destroy
    album = Album.find(params[:id])
    album.destroy
    redirect_to band_url(album.band_id)
  end


  private

  def album_params
    params.require(:album).permit(:album_name, :band_id, :type_album)
  end
end
