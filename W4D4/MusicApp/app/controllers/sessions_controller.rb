class SessionsController < ApplicationController
  def new
    render :new
  end

  def create
    user = User.find_by_credentials(login_params[:email], login_params[:password])

    if user
      login_user!(user)
      redirect_to bands_url
    else
      flash[:errors] = "invalid login"
      redirect_to new_session_url

    end
  end

  def destroy
    logout_user
    redirect_to bands_url
  end

  private
  def login_params
    params.require(:user).permit(:email, :password)
  end
end
