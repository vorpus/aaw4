class UsersController < ApplicationController
  def new
    render :new
  end

  def create
    usr = User.new(user_params)
    if usr.save
      login_user!(usr)
      redirect_to bands_url
    else
      flash[:errors] = usr.errors.full_messages
      render :new
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password)
  end
end
