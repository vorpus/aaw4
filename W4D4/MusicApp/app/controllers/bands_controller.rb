class BandsController < ApplicationController
  def index
    if logged_in?
      @bands = Band.all
      render :index
    else
      flash[:errors] = "Please log in to continue"
      redirect_to new_session_url
    end
  end

  def show
    @band = Band.find(params[:id])
    render :show
  end

  def new
    render :new
  end

  def create
    band = Band.new(band_params)
    if band.save
      redirect_to band_url(band)
    else
      flash[:errors] = band.errors.full_messages
      redirect_to bands_url
    end
  end

  def edit
    @band = Band.find(params[:id])
    render :edit
  end

  def update
    band = Band.find(params[:id])
    if band.update(band_params)
      redirect_to band_url(band)
    else
      flash[:errors] = band.errors.full_messages
      redirect_to band_url(band)
    end
  end

  def destroy
    Band.find(params[:id]).destroy
    redirect_to bands_url
  end

  private
  def band_params
    params.require(:band).permit(:name)
  end
end
