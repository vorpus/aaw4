# == Schema Information
#
# Table name: albums
#
#  id         :integer          not null, primary key
#  album_name :string           not null
#  band_id    :integer          not null
#  type_album :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Album < ActiveRecord::Base
  ALBUM_TYPES = %w(live studio)
  validates :album_name, :band_id, :type_album, presence: true
  validates :type_album, inclusion: { in: ALBUM_TYPES }

  belongs_to(
    :band,
    class_name: :Band,
    primary_key: :id,
    foreign_key: :band_id
  )

  has_many(
    :tracks,
    dependent: :destroy,
    class_name: :Track,
    primary_key: :id,
    foreign_key: :album_id
  )
end
