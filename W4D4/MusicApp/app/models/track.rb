# == Schema Information
#
# Table name: tracks
#
#  id          :integer          not null, primary key
#  track_name  :string           not null
#  album_id    :integer          not null
#  bonus_track :boolean          default(FALSE)
#  lyrics      :string           not null
#

class Track < ActiveRecord::Base
  validates :track_name, :album_id, :lyrics, presence: true
  # validates :bonus_track, inclusion: { in: [true, false] }

  belongs_to(
    :album,
    class_name: :Album,
    primary_key: :id,
    foreign_key: :album_id
  )

  has_one(
    :band,
    through: :album,
    source: :band
  )

  has_many(
    :notes,
    class_name: :Note,
    primary_key: :id,
    foreign_key: :track_id
  )
end
