# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string           not null
#  password_digest :string           not null
#  session_token   :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ApplicationRecord
  attr_reader :password
  before_validation :ensure_session_token
  validates :email, :session_token, :password_digest, presence: true
  validates :email, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true}

  has_many(
    :notes,
    class_name: :Note,
    primary_key: :id,
    foreign_key: :user_id
  )

  def self.find_by_credentials(email, password)
    user = User.find_by(email: email)
    return nil if user.nil?

    if BCrypt::Password.new(user.password_digest).is_password?(password)
      user
    else
      return nil
    end
  end



  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom.urlsafe_base64
  end

  def generate_session_token
    self.session_token = SecureRandom.urlsafe_base64
  end

  def reset_session_token!
    self.generate_session_token
    self.save!
    self.session_token
  end

end
