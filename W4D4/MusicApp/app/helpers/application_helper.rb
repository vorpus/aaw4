module ApplicationHelper
  def ugly_lyrics(lyrics)
    newlyr = "&#9835; " + lyrics.split("\r\n").join("<br>&#9835; ")
    newlyr.html_safe
  end
end
