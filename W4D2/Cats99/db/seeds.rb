# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Cat.destroy_all
cat1 = Cat.create(name: "Maggie", color: "tabby", birth_date: "2009-1-1", sex: 'F', description: 'half-feral')
Cat.create(name: "Garfield", color: "orange", birth_date: "2010-2-2", sex: 'M', description: 'loves lasagna')
Cat.create(name: "Salem", color: "black", birth_date: "1990-10-31", sex: 'M', description: 'super spooky')

CatRentalRequest.destroy_all
CatRentalRequest.create(cat_id: cat1.id, start_date: '2016-11-1', end_date: '2016-11-5', status: 'PENDING')
CatRentalRequest.create(cat_id: cat1.id, start_date: '2016-11-3', end_date: '2016-11-7', status: 'PENDING')
CatRentalRequest.create(cat_id: cat1.id, start_date: '2016-11-6', end_date: '2016-11-8', status: 'PENDING')
CatRentalRequest.create(cat_id: cat1.id, start_date: '2016-10-31', end_date: '2016-11-8', status: 'PENDING')
