class CatRentalRequestsController < ApplicationController
  def new
    render :new
  end

  def create
    req =  CatRentalRequest.new(cat_rental_request_params)
    if req.save
      redirect_to cat_url(cat_rental_request_params[:cat_id])
    else
      render json: req.errors.full_messages, status: 422
    end
  end

  def approve
    cid = CatRentalRequest.find(params[:id]).cat_id
    cat = Cat.find(cid)
    CatRentalRequest.find(params[:id]).approve!
    redirect_to cat_url(cat)
  end

  def deny
    cid = CatRentalRequest.find(params[:id]).cat_id
    cat = Cat.find(cid)
    CatRentalRequest.find(params[:id]).deny!
    redirect_to cat_url(cat)
  end


  private
  def cat_rental_request_params
    params.require(:cat_rental_request).permit(:start_date, :end_date, :cat_id, :status)
  end
end
