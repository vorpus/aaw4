class Cat < ActiveRecord::Base
  COLORS = [
    'black', 'orange','calico', 'tabby', 'white',
    'brown', 'purple', 'rose-gold'
  ]
  validates :name, presence: true
  validates :color, inclusion: { in: COLORS, message: 'not a valid cat-color' }
  validates :sex, inclusion: { in: ['M','F'], message: 'Does not adhere to gender binary' }

  has_many :cat_rental_requests,
    dependent: :destroy,
    class_name: :CatRentalRequest,
    primary_key: :id,
    foreign_key: :cat_id


  def self.colors
    COLORS
  end


end
