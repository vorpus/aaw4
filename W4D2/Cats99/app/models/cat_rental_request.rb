class CatRentalRequest < ActiveRecord::Base
  validates :status, inclusion: { in: ['PENDING','APPROVED', 'DENIED'], message: 'Not a recognized status' }
  validates :status, :start_date, :end_date, :cat_id, presence: true
  validate :overlapping_approved_requests

  belongs_to :cat,
    class_name: :Cat,
    primary_key: :id,
    foreign_key: :cat_id



  def overlapping_requests
    Cat.find(cat_id).cat_rental_requests.where.not("'#{end_date}' < start_date OR '#{start_date}' > end_date").where.not("#{id} = id")
  end

  def overlapping_approved_requests
    not_denied_rental_reqs = Cat.find(cat_id).cat_rental_requests.order(:start_date).where("status = 'APPROVED'")
    not_denied_rental_reqs.each do |req|
      if start_date <= req.start_date
        if end_date >= req.start_date
          return false
        end

      end
      if start_date >= req.start_date
        if start_date <= req.end_date
          return false
        end
      end
    end
    true
  end

  def approve!
    ActiveRecord::Base.transaction do
      CatRentalRequest.find(id).update(:status => "APPROVED")
      CatRentalRequest.find(id).overlapping_requests.each do |req|
        req.deny!
      end
    end
  end

  def deny!
    CatRentalRequest.find(id).update(:status => "DENIED")
  end

  def pending?
    return true if CatRentalRequest.find(id).status == "PENDING"
    false 
  end
end
