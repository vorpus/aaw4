class SessionsController < ApplicationController
  def new
    if logged_in?
      redirect_to cats_url
    else
      render :new
    end
  end

  def create
    @user = User.find_by_credentials(
      session_params[:user_name],
      session_params[:password]
    )
    if @user
      session[:session_token] = @user.reset_session_token!
      redirect_to cats_url
    else
      flash[:errors] = ["Incorrect login info"]
      redirect_to new_session_url
    end
  end

  def destroy
    unless current_user.nil?
      current_user.reset_session_token!
      session[:session_token] = nil
    end
    redirect_to cats_url
  end

  private

  def session_params
    params.require(:user).permit(:user_name, :password)
  end
end
