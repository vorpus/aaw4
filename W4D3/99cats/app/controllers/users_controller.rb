class UsersController < ApplicationController

  def new
    if logged_in?
      redirect_to cats_url
    else
      render :new
    end
  end

  def create
    user = User.new(user_params)

    if user.save
      session[:session_token] = user.session_token
      redirect_to cats_url
    else
      render json: user.errors.full_messages
    end
  end

  private

  def user_params
    params.require(:user).permit(:user_name, :password)
  end
end
