# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  user_name       :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)      not null
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  before_validation :ensure_session_token
  validates :user_name, :password_digest, presence: true
  validates :user_name, uniqueness: true
  validates :session_token, presence: true, uniqueness: true

  has_many(
    :cats,
    class_name: :Cat,
    primary_key: :id,
    foreign_key: :user_id
  )

  has_many(
    :cat_rental_requests,
    class_name: :CatRentalRequest,
    primary_key: :id,
    foreign_key: :user_id
  )

  attr_reader :password

  def reset_session_token!
    self.session_token = SecureRandom.urlsafe_base64
    self.save!
    self.session_token
  end

  def ensure_session_token
    self.session_token ||= SecureRandom.urlsafe_base64
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.find_by_credentials(user_name, password)
    @user = User.find_by(user_name: user_name)
    @user && @user.is_password?(password) ? @user : nil
  end


end
