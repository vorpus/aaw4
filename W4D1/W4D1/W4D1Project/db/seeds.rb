# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
User.destroy_all
User.create(username: "Bruce Wang")
User.create(username: "Li Zhang")
User.create(username: "Gizmo")

Contact.destroy_all
Contact.create(name: "Bruce mom", email: "bmommy@g.com", user_id: User.find_by_username("Bruce Wang").id)
Contact.create(name: "Bruce dad", email: "bdaddy@g.com", user_id: User.find_by_username("Li Zhang").id)
Contact.create(name: "Gus", email: "abc@g.com", user_id: User.find_by_username("Bruce Wang").id)

ContactShare.destroy_all
ContactShare.create(contact_id: Contact.first.id, user_id: User.find_by_username("Li Zhang").id)
ContactShare.create(contact_id: Contact.first.id, user_id: User.find_by_username("Gizmo").id)
