Rails.application.routes.draw do
  # resources :users

  # get '/users', to: 'users#index'
  # get '/users/:id', to: 'users#show'
  # post '/users', to: 'users#create'
  # get 'users/new', to: 'users#new'
  # patch 'users/:id', to: 'users#update'
  # delete 'users/:id', to: 'users#destroy'

  resources :users, only: [:index, :show, :create, :update, :destroy]

  resources :contacts, only: [:show, :create, :update, :destroy]

  resources :comments, only: [:show, :create, :update, :destroy]

  resources :users do
    resources :contacts, only: :index
    resources :comments, only: :index
  end

  resources :contacts do
    resources :comments, only: :index
  end

  resources :contact_shares, only: [:create, :destroy]


end
