class ContactSharesController < ApplicationController
  def create
    contactshare = ContactShare.new(contact_share_params)

    if contactshare.save
      render json: contactshare
    else
      render json: contactshare.errors.full_messages, status: 422
    end
  end

  def destroy
    contactshare = ContactShare.find(params[:id])
    if contactshare.destroy
      render json: contactshare
    else
      render json: contactshare.errors.full_messages, status: 422
    end
  end

  private
  def contact_share_params
    params.require(:contact_share).permit(:contact_id, :user_id)
  end
end
