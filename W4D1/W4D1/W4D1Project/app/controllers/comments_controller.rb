require 'byebug'
class CommentsController < ApplicationController

  def index
    key = params.keys.last
    model_name = params.keys.last.gsub(/_id/, "").capitalize.constantize
    all_comments = model_name.find(params[key.to_sym]).comments
    # debugger
    render json: all_comments
  end

  def create
    comment = Comment.new(comment_params)
    if comment.save
      render json: comment
    else
      render json: comment.errors.full_messages, status: 422
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:body, :commentable_id, :commentable_type)
  end
end
