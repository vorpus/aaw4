class Contact < ActiveRecord::Base

  validates :name, :email, presence: true
  validates :user_id, uniqueness: {scope: :email}

  #who owns the contact info
  belongs_to :owner,
    class_name: :User,
    primary_key: :id,
    foreign_key: :user_id

    #contact share links
  has_many :contact_shares,
    dependent: :destroy,
    class_name: :ContactShare,
    primary_key: :id,
    foreign_key: :contact_id

    #users who have the contact info
  has_many :shared_users,
    through: :contact_shares,
    source: :user

  has_many :comments, as: :commentable
end
