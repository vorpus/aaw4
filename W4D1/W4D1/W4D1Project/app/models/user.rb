class User < ActiveRecord::Base

  validates :username, presence: true

  has_many :contacts,
    dependent: :destroy,
    class_name: :Contact,
    primary_key: :id,
    foreign_key: :user_id

    #contacts that the User has
  has_many :contact_shares,
    class_name: :ContactShare,
    primary_key: :id,
    foreign_key: :user_id

    #contacts shared to YOU
  has_many :shared_contacts,
    through: :contact_shares,
    source: :contact

  has_many :comments, as: :commentable
end
